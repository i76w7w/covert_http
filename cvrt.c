#include"cvrt.h"
#include<sys/socket.h>
#include<stdio.h>
u_short parse_ipid(const u_char *p)
{
	u_short *ipid=(u_short *)(p+18);
	return *ipid;
}

u_char parse_bit(u_short ipid1,u_short ipid2,u_short noise)
{
	if (ipid1==0)
		return 2;
	else{
		if( (ipid2-ipid1) >= (noise-1) )
			return 1;
		else
			return 0;
	}
}

void authernate(Auth *auth,u_short bit)
{
	u_short t =( (auth->password[auth->pa]) >> (auth->pb) )&1;
	if(t==bit){
		auth->pb++;
		if (auth->pb == 8){
			auth->pa++;
			auth->pb=0;
		}
		if(auth->pa==auth->passlen){
			auth->flag=1;
			auth->pa=0;
		}
	}else{
		if (auth->first1==0){
			int i;
			for(i=0;( ((auth->password[0])>>i) & 1 )==0;i++)
				continue;
			auth->pb=i;
			auth->pa=0;
			return ;
		}
		auth->pa=0;
		auth->pb=0;
		auth->first1=0;
	}
}

void build_byte(Character *ch,u_short bit)
{
	if(bit==1)
		ch->c += bit<<(ch->p);
	ch->p++;
	if(ch->p == 8){
		ch->flag=1;
		ch->p=0;
	}
}

void bit1(int times,sender *snd)
{
	int i;
	char buf[HTTP_PAYLOAD_SIZE];
	for (i = 0; i < times; i++){
		Send(snd->sock, snd->buf, snd->buflen, snd->flags);
		Recv(snd->sock, buf, snd->buflen, snd->flags);
	}
}
void bit0()
{
	return;
}
