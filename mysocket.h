#include<sys/socket.h>
#include<netinet/in.h>
#define PORT 80
#define HTTP_PAYLOAD_SIZE 1024
#define SOCKET int
typedef struct{
	SOCKET sock;
	char *buf;
	int buflen;
	int flags;
}sender;
int Socket(int family, int type, int protocol);
int Connect(SOCKET sock, struct sockaddr_in *server_addr, int size);
int Send(SOCKET socket,char *buf,int len,int flags);
int Recv(SOCKET socket,char *buf,int len,int flags);
