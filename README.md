You can also check out [my blog][5] about this project.
#Introduction
---
This is the http version of covert communication tool. Check out the ICMP implemented version [here][1]. 

As indicated by its name, this tool allows a couple of host to communicate covertly despite network traffic retention. This tool is the implementation of the ideas prompted in [this article][2], in which you can also learn more about the principle of this tool. If you don't want to read the paper above, and just want to know the usage, read the following abstract.

To communicate covertly, for A and B, they need a server C to help. The server can be a normal website which don't need to do any retransmission or something. We want the server to maintain a continous ipid (the id part in IP header, see [here][4]) stack and the B change the ipid with his mind (by sending http requests) . Observing the changing made by B (also by sending http requests ) , A can infer something from it. So we can say that we have encoded the message in the change of the ipid stack. In this way, B can send message to A covertly. 
#Compile
---
This tool needs pcap. Make sure you have pcap installed first. 

Having pcap, just run`make`for compiling and `make clean`to delete the ouputs.

Just check out the makefile. It is really simple.

#Usage
---
After compilation, there are three new generated binary files, whose names are capture, snd and sender. Run them without parameters and you will see the usage. Or just read the instructions below.

1. capture
---
./capture needs two parameters, the first one is the server's ip and the second one is the ip of yourself.
```bash
$ ifconfig
...
inet addr : xxx.xxx.xxx.xxx
...
$ ./capture yyy.yyy.yyy.yyy xxx.xxx.xxx.xxx
```
2. snd 
---
./snd needs only one parameter which is the specific page that you want to [GET][3] from the server. 
```
$ ./snd http://yyy.yyy.yyy.yyy/covert.html
```
3. sender
---
./sender needs two parameters and the first one is the same as snd's parameter, but you'd better choose a different page which is smaller than the one used in snd.
And the second one is the message that you want to transmit. 

As is this tool is a part of another project in which it needs this tool to transmit a ip address covertly, the message is designed to be an ip address. In the program, I encoded the 15 byes long string "xxx.xxx.xxx.xxx" into a 4 byes long string. And I added a 3 byte long string for authorization ("XDU" is now used in the program). So the program ends up sending a 7 bytes long string. 
```
$ ./sender http://yyy.yyy.yyy.yyy/covert1.html 192.168.1.1
```
[1]:https://bitbucket.org/i76w7w/covert_communication
[2]:http://dl.acm.org/citation.cfm?id=2022842
[3]:http://en.wikipedia.org/wiki/GET_%28HTTP%29#Request_methods
[4]:http://en.wikipedia.org/wiki/IPv4#Packet_structure
[5]:http://i76w7w.bitbucket.org/2014-03-09.html