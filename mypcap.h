int Pcap_findalldevs(pcap_if_t **alldevs,char *errbuf);
void Pcap_printalldevs(pcap_if_t *alldevs);
char *Choose(pcap_if_t *alldevs);
pcap_t *Pcap_open_live(char *dev_name,int size,int promisc,int t_to_ms,char *errbuf);
int Pcap_lookupnet(char *dev_name,bpf_u_int32 *ip,bpf_u_int32 *netmask,char *errbuf);
int Pcap_compile(pcap_t *handle,struct bpf_program *fp,char *filter_exp,int optimize,int netmask);
int Pcap_setfilter(pcap_t *handle,struct bpf_program *fp);
