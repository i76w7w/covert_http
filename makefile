all : capture snd sender
#######Build capture#######
capture : capture.o mypcap.o cvrt.o mysocket.o
	gcc -o capture capture.o mypcap.o cvrt.o mysocket.o -lpcap
capture.o : capture.c
	gcc -c capture.c -g -Wall
mypcap.o : mypcap.c mypcap.h
	gcc -c mypcap.c -g -Wall
mysocket.o : mysocket.c mysocket.h
	gcc -c mysocket.c -g -Wall
cvrt.o : cvrt.c cvrt.h
	gcc -c cvrt.c -g -Wall
#######Build snd#######
snd : snd.o mysocket.o
	gcc -o snd snd.o mysocket.o
snd.o : snd.c
	gcc -c snd.c -g -Wall
#######Build sender#######
sender : sender.o mysocket.o cvrt.o
	gcc -o sender sender.o mysocket.o cvrt.o
sender.o : sender.c
	gcc -c sender.c -g -Wall
clean :
	rm -rf capture capture.o mypcap.o cvrt.o \
	snd snd.o mysocket.o \
	sender sender.o
