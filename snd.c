#include<sys/socket.h>
#include<sys/types.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<netdb.h>
#include"mysocket.h"
#include<stdio.h>
#include<unistd.h>
#include<string.h>

typedef struct{
	unsigned long ip;
	int port;
}argument;

char http_payload_format[] = "GET %s HTTP/1.1\r\n"
"Accept: image/gif, image/jpeg, */*\r\nAccept-Language: zh-cn\r\n"
"Accept-Encoding: gzip, deflate\r\nHost: %s:%d\r\n"
"User-Agent: covert <0.1>\r\nConnection: Keep-Alive\r\n\r\n";
char usage[]="[Usage]\n"
		"snd url\n"
		"[Example]\n"
		"snd http://192.168.1.107/covert1.html\n";
char ip_dot_format[]="%d.%d.%d.%d";

void parse_arguments(argument *arg, int argc, char **argv);

int main(int argc, char ** argv)
{
	if (argc < 2){
		printf("%s",usage);
		return -1;
	}
	int sleeptime;
	printf("sleeptime=");
	scanf("%d",&sleeptime);
	/***********************************************************************


	                        Prepare for Sending...


	***********************************************************************/
	argument arg={0};
	parse_arguments(&arg, argc, argv);

	int sock;

	struct sockaddr_in server_addr;
	server_addr.sin_family = AF_INET;
	server_addr.sin_port = htons(arg.port);
	server_addr.sin_addr.s_addr = arg.ip;

	/* Generate HTTP payload */
	char buf[HTTP_PAYLOAD_SIZE];
	char ip_dot[16]={0};
	u_char *p_ip=(u_char *)(&(arg.ip));
	sprintf(ip_dot,ip_dot_format,p_ip[0],p_ip[1],p_ip[2],p_ip[3]);
	int buf_len = sprintf(buf,http_payload_format,argv[1],ip_dot,arg.port);
	/***********************************************************************


	                                Sending...


	***********************************************************************/
//	int i =10;
	sock = Socket(AF_INET, SOCK_STREAM, 0);
	Connect(sock, &server_addr, sizeof(server_addr));
	while (1){
		Send(sock,buf,buf_len,0);
		sleep(sleeptime);
		printf("\n");
	}
}
void parse_arguments(argument *arg, int argc, char **argv)
{
	char *server = argv[1] + 7;
	int i;
	for (i = 7; argv[1][i]; i++){
		if (argv[1][i] == '/')
			break;
	}
	argv[1][i] = 0;
	unsigned long a = inet_addr(server);
	struct hostent *host_ent;
	if (a == INADDR_NONE){
		host_ent = gethostbyname(server);
		if (host_ent == NULL){
			fprintf(stderr,"invalid server!\n");
			return;
		}
		memcpy(&a, host_ent->h_addr_list[0], host_ent->h_length);
	}
	arg->ip = a;
	arg->port = 80;
	argv[1][i] = '/';
}
