#include<stdio.h>
#include<sys/socket.h>
#include<sys/types.h>
#include"mysocket.h"
#include<errno.h>
int Socket(int family, int type, int protocol)
{
	int n = socket(family, type, protocol);
	if (n == -1)
		fprintf(stderr,"failed to create a socket.\n");
	return n;
}
int Connect(int sock, struct sockaddr_in *server_addr, int size)
{
	int flag=connect(sock, (struct sockaddr *)server_addr, size);
	if (flag == -1)
		printf("failed to connect.\n");
	return flag;
}
int Send(SOCKET socket, char *buf, int len, int flags)
{
	int result = send(socket, buf, len, flags);
	if (result == -1){
		printf("failed to send[unknown].\n");//,WSAGetLastError());
		return -1;
	}
	return result;
}
int Recv(SOCKET socket, char *buf, int len, int flags)
{
	int result = recv(socket, buf, len, flags);
	if (result == -1){
		printf("failed to receive.\n");
		return -1;
	}
	return result;
}
int Close(int sock){
	int n = close(sock);
	if (n==-1)
		printf("failed to close[%d].\n",errno);
	return n;
}
