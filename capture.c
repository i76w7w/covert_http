#include<pcap/pcap.h>
#include<arpa/inet.h>
#include<stdio.h>
#include<string.h>
#include"mypcap.h"
#include"cvrt.h"

char filter_format[]="tcp and src net %s and dst net %s";
char usage[]="[Usage]\n"
		"pcap server_ip host_ip\n"
		"[Example]\n"
		"pcap 192.168.1.107 192.168.1.105\n";
char filter[100]={0};

u_short IPID=0;
Auth auth={"XDU",strlen("XDU"),0};
Character character[4]={0};
int characters=0;

void handler(u_char *args,
		const struct pcap_pkthdr* pkthdr,
		const u_char *packet);

int main(int argc,char **argv)
{
	if(argc!=3){
		printf("%s",usage);
		return -1;
	}
	pcap_if_t *alldevs;
	pcap_t *dev;
	bpf_u_int32 ip,netmask;
	char *dev_name=NULL;
	char errbuf[PCAP_ERRBUF_SIZE];

	Pcap_findalldevs(&alldevs,errbuf);
	Pcap_printalldevs(alldevs);
	dev_name = Choose(alldevs);
	Pcap_lookupnet(dev_name,&ip,&netmask,errbuf);
	dev = Pcap_open_live(dev_name,BUFSIZ,0,2000,errbuf);

	sprintf(filter,filter_format,argv[1],argv[2]);
	printf("%s\n",filter);
	struct bpf_program fp;
	Pcap_compile(dev, &fp, filter,0,netmask);
	Pcap_setfilter(dev,&fp);

	pcap_loop(dev,-1,handler,NULL);

	return 0;
}
void handler(u_char *args,
		const struct pcap_pkthdr* pkthdr,
		const u_char *packet)
{
	u_short *ipid=(u_short *)(packet+18);
	int bit = parse_bit(IPID,htons(*ipid),5);
	IPID=htons(*ipid);
	switch (bit){
	case 0:printf("0");break;
	case 1:printf("1");break;
	case 2:printf("Start:\n");return;
	default:printf("Receive Bit Error!\n");
	}
	if(bit==1 && auth.first1==0) auth.first1=1;
	printf("[%d][%d,%d]\n",IPID,auth.pa,auth.pb);
	fflush(stdout);

	if (auth.flag == 0){
		authernate(&auth,bit);
		if(auth.flag)
			printf("\nReceiving:\n");
		return;
	}

	build_byte(character+characters,bit);
	if(character[characters].flag){
		printf("%d.\n",character[characters].c);
		characters++;
		if (characters==4){
			FILE *fout=fopen("ip.txt","w");
			fprintf(fout,"%d.%d.%d.%d",
					character[3].c,
					character[2].c,
					character[1].c,
					character[0].c);
			fclose(fout);
			exit(0);
		}
	}
}
