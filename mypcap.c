#include<pcap/pcap.h>
#include<stdio.h>
int Pcap_findalldevs(pcap_if_t **alldevs,char *errbuf)
{
	int n=pcap_findalldevs(alldevs,errbuf);
	if (n == -1)
		fprintf(stderr,"Pcap_findalldevs() failed! \n%s\n",errbuf);
	if (*alldevs == NULL)
		printf("Find no device! \n");
	return n;
}
void Pcap_printalldevs(pcap_if_t *alldevs)
{
	int i;
	for(i=1;alldevs->next;i++){
		printf("%d.[%s]:%s\n",i,alldevs->name,alldevs->description);
		alldevs=alldevs->next;
	}
	return;
}
char *Choose(pcap_if_t *alldevs)
{
	char *name;
	int i;
	int n;
	printf("Please Enter A number: ");
	scanf("%d",&n);
	for (i=1;alldevs->next;i++){
		if (i==n)
			name=alldevs->name;
		alldevs=alldevs->next;
	}
	if(name == NULL)
		fprintf(stderr,"Failed to choose a device!\n");
	return name;
}
pcap_t *Pcap_open_live(char *dev_name,int size,int promisc,int t_to_ms,char *errbuf)
{
	pcap_t *n = pcap_open_live(dev_name,size,promisc,t_to_ms,errbuf);
	if (n == NULL)
		fprintf(stderr,"Pcap_open_live() failed! \n%s\n",errbuf);
	return n;
}
int Pcap_lookupnet(char *dev_name,bpf_u_int32 *ip,bpf_u_int32 *netmask,char *errbuf)
{
	int n = pcap_lookupnet(dev_name,ip,netmask,errbuf);
	if (n == -1 )
		fprintf(stderr,"Pcap_lookupnet() failed! \n%s\n",errbuf);
	return n;
}
int Pcap_compile(
		pcap_t *handle,
		struct bpf_program *fp,char *filter_exp,
		int optimize,int netmask)
{
	int n=pcap_compile(handle, fp, filter_exp, optimize, netmask);
	if (n == -1)
		fprintf(stderr,"Pcap_compile() failed! \n%s\n",pcap_geterr(handle));
	return n;
}
int Pcap_setfilter(pcap_t *handle,struct bpf_program *fp){
	int n = pcap_setfilter(handle, fp);
	if (n == -1)
		fprintf(stderr,"Pcap_setfilter() failed!\n%s\n",pcap_geterr(handle));
	return n;
}
