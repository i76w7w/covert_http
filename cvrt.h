//#include<WinSock2.h>
#include"mysocket.h"
#include<sys/socket.h>
#include<pcap/pcap.h>
typedef struct{
	char *password;
	int passlen;
	int flag;
	int first1;
	int pa;
	int pb;
}Auth;
typedef struct{
	unsigned char c;
	int p;
	int flag;
}Character;

u_short parse_ipid(const u_char *p);
u_char parse_bit(u_short ipid1,u_short ipid2,u_short noise);
void authernate(Auth *auth,u_short bit);
void build_byte(Character *ch,u_short bit);
/*Send a bit by calling func Sender() for "times" times.*/
/*snd is a package of info about target message,used by Send()*/
void bit1(int times,sender *snd);
void bit0();
/*Send a byte by dividing them into bits and then call bit1() or bit0()
Bits are sended from low to high.
c--the sended character 
sleeptime--milli seconds between bits 
times&snd--needed by bit1()*/
//void send_byte(unsigned char c,int sleeptime, int times, sender* snd);

